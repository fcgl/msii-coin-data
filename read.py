import numpy as np
from plyfile import PlyData, PlyElement
import matplotlib.pyplot as plt
from pathlib import Path

def read_features(fileName : Path) -> np.ndarray:
    '''
    fileName path to PLY file generated with GigaMesh on a mesh
             with a regular grid in x and y
    return MSII feature array with shape (rows, cols, 16) 
    '''
    data = PlyData.read(fileName)
    if data.comments[1][24:32] != 'GigaMesh':
        raise RuntimeError('{} not generated with GigaMesh'.format(fileName))
    if len(data._elements[0].properties) != 13:
        raise RuntimeError('There are not 13 vertex properties in {}'.format(fileName))
    if data._elements[0].properties[-1].name != 'feature_vector':
        raise RuntimeError('Last vertex property in {} is not feature_vector'.format(fileName))
    x = np.asarray([x[0] for x in data['vertex'].data.tolist()])
    y = np.asarray([x[1] for x in data['vertex'].data.tolist()])
    f = np.asarray([x[-1] for x in data['vertex'].data.tolist()])
    x, ix = np.unique(x, return_inverse=True)
    y, iy = np.unique(y, return_inverse=True)
    rows = y.size
    cols = x.size
    n = f.shape[1]
    idx = ix + iy * cols
    feat = np.empty((rows*cols,n))
    feat.fill(np.nan)
    feat[idx, :] = f
    feat = np.reshape(feat, (rows, cols, n))
    feat = np.flip(feat, axis=0)
    return feat

if __name__ == '__main__':
    fileName = Path('./example/8818_03_34_2021-03-09_14-15-57_mesh.ply')
    feat = read_features(fileName)
    # the feature channels correspond to decreasing ball radii 
    # in this case 0.75 * [16, 15, ..., 1]/16 mm
    # One can pick e.g. the first channel and segment the region with
    # less intersected sphere volume below the coin surface than above
    t = np.squeeze(feat[:,:,0])
    im = t < 0 & ~np.isnan(t)
    plt.imshow(im, cmap='gray')
    plt.show()
